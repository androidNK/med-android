package com.medicament.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.medicament.R;
import com.medicament.exception.NotInitialisedException;

/**
 * SharedPreferenceManager
 * Created by Kamran on 12/9/17.
 */

public class SharedPreferenceManager {

    private static SharedPreferenceManager sharedInstance;
    private SharedPreferences sharedPreferences;
    private static final String exceptionMessage = "SharedPreferenceManager is not initialised.\n Initialise in Application Class, Syntax to initialise \"SharedPreferenceManager.init(getApplicationContext());";

    private final static String APP_LANGUAGE_CODE = "app_language_code";
    private final static String APP_LANGUAGE_NAME = "app_language_name";

    private SharedPreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public static synchronized void init(Context context) {
        if (sharedInstance == null)
            sharedInstance = new SharedPreferenceManager(context);
    }

    public static SharedPreferenceManager getInstance() {
        if (sharedInstance == null)
            throw new NotInitialisedException(exceptionMessage);

        return sharedInstance;
    }

    /**
     * Method to set boolean in preferences
     */
    private boolean setBooleanInPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    /**
     * Method to set string in preferences
     */
    private boolean setStringInPreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value == null ? "" : value);
        return editor.commit();
    }

    /**
     * Method to get string from preferences
     */
    private String getStringFromPreferences(String key) {
        return sharedPreferences.getString(key, "");
    }

    /**
     * Method to set boolean in preferences
     */
    private boolean setIntegerInPreferences(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    /**
     * Method to get boolean from preferences
     */
    private int getIntegerFromPreferences(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    /**
     * Method to get boolean from preferences
     */
    private boolean getBooleanFromPreferences(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void saveNeverAskAgainStatus(String permission, boolean value) {
        setBooleanInPreferences(permission, value);
    }

    public boolean getNeverAskAgainStatus(String permission) {
        return getBooleanFromPreferences(permission);
    }

    public void saveAppLanguageCode(String languageCode) {
        setStringInPreferences(APP_LANGUAGE_CODE, languageCode);
    }

    public String getAppLanguageCode() {
        return getStringFromPreferences(APP_LANGUAGE_CODE);
    }
}
