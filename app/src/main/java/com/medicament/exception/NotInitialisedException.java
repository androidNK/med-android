package com.medicament.exception;

/**
 * Created by Kamran on 22/08/17.
 * NotInitialisedException is RuntimeException if user don't initialise the LhtLogger
 */
public class NotInitialisedException extends RuntimeException {

    public NotInitialisedException(String message) {
        super(message);
    }
}