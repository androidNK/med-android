package com.medicament;

import android.app.Application;

/**
 * MedicamentApp
 * Created by Kamran on 12/9/17.
 */

public class MedicamentApp extends Application {

    private static MedicamentApp appInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
    }

    public static MedicamentApp getAppInstance() {
        return appInstance;
    }
}
