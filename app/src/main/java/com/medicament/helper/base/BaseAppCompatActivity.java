package com.medicament.helper.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.medicament.R;

/**
 * BaseAppCompatActivity
 * Created by Kamran on 12/9/17.
 */

public abstract class BaseAppCompatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        int layoutResource = getLayoutResource();

        if (layoutResource == R.layout.activity_splash) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
        super.onCreate(savedInstanceState);
        setContentView(layoutResource);
        Log.d("KAM", "BaseAppCompatActivity-TEST");
        onActivityLaunched();
    }

    /**
     * Launch the new Activity and finish the current activity
     * @param activity class
     * @param finish boolean
     */
    protected void launchActivityFinish(final Class<?> activity, final boolean finish){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BaseAppCompatActivity.this,activity);
                startActivity(intent);
                if(finish)
                    finish();
            }
        });
    }

    /**
     * Launch the new Activity and finish the current activity and there stack.
     * @param activity class
     * @param finishAffinity boolean
     */
    protected void launchActivityFinishAffinity(final Class<?> activity, final boolean finishAffinity){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BaseAppCompatActivity.this,activity);
                startActivity(intent);
                if(finishAffinity)
                    finishAffinity();
            }
        });
    }

    protected abstract int getLayoutResource();

    protected abstract void onActivityLaunched();
}
