package com.medicament.module.selectlanguage;

import com.medicament.R;
import com.medicament.helper.base.BaseAppCompatActivity;

/**
 * Created by Kamran on 12/9/17.
 */

public class SelectLanguageActivity extends BaseAppCompatActivity {
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_select_language;
    }

    @Override
    protected void onActivityLaunched() {

    }
}
