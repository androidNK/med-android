package com.medicament.module.splash;

import android.os.Handler;

import com.medicament.R;
import com.medicament.helper.base.BaseAppCompatActivity;
import com.medicament.module.selectlanguage.SelectLanguageActivity;

public class SplashActivity extends BaseAppCompatActivity {

    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onActivityLaunched() {
        int splashDelay = 3000;
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                launchActivityFinish(SelectLanguageActivity.class,true);
            }
        }, splashDelay);
    }
}
