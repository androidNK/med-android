package com.medicament.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.medicament.manager.SharedPreferenceManager;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Developer on 5/16/2017.
 * Application Utils Class
 */
public class AppUtils {
    public static final String PSK = "PSK";
    public static final String WEP = "WEP";
    public static final String OPEN = "Open";

    private static Toast toast;
    private static Snackbar snackbar;
    private static SparseArray<String> sourceNameSparseArray;


    public static boolean isEmpty(@Nullable String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * @param activity Activity
     * @param classIns Class
     */
    public static void restartApplication(Activity activity, Class<?> classIns) {

        ActivityCompat.finishAffinity(activity);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * Disble touch for window
     *
     * @param context Context
     */
    public static void disableTouchEvent(Context context) {
        Activity activity = (Activity) context;
        if (activity != null)
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    /**
     * Enable touch for window
     *
     * @param context Context
     */
    public static void enableTouchEvent(Context context) {
        Activity activity = (Activity) context;
        if (activity != null)
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    /**
     * Hide Softkeyboad
     *
     * @param context Context
     */
    public static void hideSoftKeyboard(Context context) {
        toggleSoftKeyboard(context, null, false);
    }


    public static void hideSoftKeyboard(Context context, EditText editText) {
        toggleSoftKeyboard(context, editText, false);
    }

    public static void showSoftKeyboard(Context context, EditText editText) {
        toggleSoftKeyboard(context, editText, true);
    }


    /**
     * @param context Context
     * @param view    View
     * @param show    boolean
     */
    private static void toggleSoftKeyboard(Context context, View view, boolean show) {
        if (context == null)
            return;

        if (view == null)
            view = ((Activity) context).getCurrentFocus();

        if (view == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager == null)
            return;

        if (show) {
            view.requestFocus();
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        } else {
            IBinder iBinder = view.getWindowToken();
            if (iBinder == null)
                return;

            inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
            view.clearFocus();
        }
    }

    /**
     * @param context Context
     * @param message String
     */
//    public static void showToastAtBottom(Context context, String message) {
//        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
//    }
    public static void showToastAtBottom(Context context, String message) {
        if (message == null)
            return;

        try {
            toast.getView().isShown();     // true if visible
            toast.setText(message);
        } catch (Exception e) {         // invisible if exception
            toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
//            LHTLogger.instance().error("Kamran", "AppUtils", "showToastAtBottom", e.getMessage(), Arrays.toString(e.getStackTrace()), false);
        } finally {
            if (toast != null)
                toast.show();
        }

    }

    public static void showErrorToast(Context context, String message) {
        showToastAtBottom(context, message);
    }

    public static void showDevelopmentToast(Context context, String message) {
//        if (BuildConfig.BUILD_TYPE.equals(AppConstants.BUILD_DEV))
        showToastAtBottom(context, message);
    }

    public static void showSnackBarAtBottom(Context context, String message) {
        if (message == null)
            return;

        try {
            snackbar.getView().isShown();
            snackbar.setText(message);
        } catch (Exception e) {
            snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, BaseTransientBottomBar.LENGTH_LONG);
//            LHTLogger.instance().error("Kamran", "AppUtils", "showSnackBarAtBottom", e.getMessage(), Arrays.toString(e.getStackTrace()), false);
        }
        if (snackbar != null)
            snackbar.show();
    }


    @SuppressLint("HardwareIds")
    public static String getDeviceUniqueId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static float getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static float getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }


    public static boolean equalsBytes(byte[] array1, byte[] array2) {
        if (array1 == null || array2 == null || array1.length != array2.length)
            return false;

        for (int i = 0; i < array1.length; i++)
            if (array1[i] != array2[i])
                return false;

        return true;
    }

    /**
     * Convert milliSeconds to String time {mm:ss}
     *
     * @param time long
     * @return String
     */
    public static String convertMilliSecondsToTimeString(long time) {
        int seconds = (int) time % 60;
        int minutes = (int) (time / 60) % 60;
        if (seconds < 10)
            return minutes + ":0" + seconds;
        return minutes + ":" + seconds;
    }


    /**
     * Set Locale for Application
     *
     * @param context      Context
     * @param languageCode String
     */
    public static void setApplicationLocale(Context context, String languageCode) {
        if (context == null || languageCode == null || isEmpty(languageCode))
            return;
        Locale locale = null;
        if (languageCode.contains("_")) {
            String[] split = languageCode.split("_");
            if (split.length >= 2) {
                locale = new Locale(split[0], split[1]);
            }
        } else {
            locale = new Locale(languageCode);
        }
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.setLocale(locale);
        context.createConfigurationContext(configuration);
    }


    /* Converting a String to Quoted String
        * Syntax for Setting SSID in wifi Conf : / SSIDNAME /*/
    public static String convertToQuotedString(String string) {
        if (isEmpty(string)) {
            return "";
        }
        final int lastPos = string.length() - 1;
        if (lastPos > 0 && (string.charAt(0) == '"' && string.charAt(lastPos) == '"')) {
            return string;
        }
        return "\"" + string + "\"";
    }


    public static Typeface getTypefaceInstance(Context context, @StringRes int font) {
        return Typeface.createFromAsset(context.getAssets(), context.getString(font));
    }

    public static void openExternalApp(Context context, String packageName, String deepLink) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            intent = new Intent(Intent.ACTION_VIEW);
            try {
                intent.setData(Uri.parse("market://details?id=" + packageName));
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + packageName));
                context.startActivity(intent);
            }
        } else {
            if (deepLink != null && !isEmpty(deepLink)) {
                intent = new Intent(packageName + "." + deepLink);
//                intent.putExtra(packageName + ".extra.IP_ADDRESS", EtonSharePreferenceManager.getInstance().getCurrentSpeakerIPAddress());
            }

            context.startActivity(intent);
        }
    }

    public static void openMobileSetting(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static String getTimeFromMinutesByte(byte hostDataByte) {
        byte hrs = 60;
        if (hostDataByte < hrs)
            return hostDataByte + " Min";
        long hours = TimeUnit.MINUTES.toHours(hostDataByte);
        long remainMinute = hostDataByte - TimeUnit.HOURS.toMinutes(hours);

        return String.format(Locale.US, "%01d:%02d", hours, remainMinute) + " Hr";
    }

    public static String replaceAlphabets(String string) {
        if (isEmpty(string))
            return "";

        return string.replaceAll("[a-zA-z]", "");
    }

    public static void setAppLocale(@NonNull Context context, @NonNull String languageCode, String languageName) {
        if (languageCode.isEmpty())
            return;
        SharedPreferenceManager.getInstance().saveAppLanguageCode(languageCode);
        Locale locale = null;
        Resources resources = context.getResources();
        if (languageCode.contains("_")) {
            String[] split = languageCode.split("_");
            if (split.length >= 2) {
                locale = new Locale(split[0], split[1]);
            }
        } else
            locale = new Locale(languageCode);

        Locale.setDefault(locale);
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }


    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm == null)
                return false;

            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
//            LHTLogger.instance().error("Kamran", "EtonBaseCompatActivity", "isOnline", e.getMessage(), Arrays.toString(e.getStackTrace()), false);
            return false;
        }
    }


}
