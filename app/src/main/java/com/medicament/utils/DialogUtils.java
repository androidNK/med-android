package com.medicament.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.Spanned;

import com.medicament.R;

/**
 * Created by Developer on 6/8/2017.
 * DialogUtils class is used to show Alert/Message Dialog
 */
public class DialogUtils {

    public static AlertDialog showAlertDialog(Context context, @StringRes int title, @StringRes int message,
                                              @StringRes int positiveText, @StringRes int negativeText,
                                              boolean showCancelButton, boolean isCancelable,
                                              final DialogCallback callback) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(context);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
                        if (callback != null)
                            callback.onPositiveClicked(dialog);
                    }
                });
        if (showCancelButton) {
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
                    if (callback != null)
                        callback.onNegativeClicked(dialog);
                }
            });
        }

        return builder.show();
    }

    public static AlertDialog showAlertDialog(Context context, String title, String message,
                                              @StringRes int positiveText, @StringRes int negativeText,
                                              boolean showCancelButton, boolean isCancelable,
                                              final DialogCallback callback) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new android.support.v7.app.AlertDialog.Builder(context);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        title = title == null ? context.getString(R.string.app_name) : title;
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
                        if (callback != null)
                            callback.onPositiveClicked(dialog);
                    }
                });
        if (showCancelButton) {
            builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
                    if (callback != null)
                        callback.onNegativeClicked(dialog);
                }
            });
        }
        return builder.show();
    }


    public static void showPermissionDialog(Context context, Spanned title, String message, String positiveText, String negativeText, Drawable resId, final DialogCallback callback) {
        /*MaterialDialog.Builder materialBuilder = new MaterialDialog.Builder(context);

        materialBuilder.title(title)
                .content(message)
                .positiveText(positiveText)
                .cancelable(false);

        if (resId != null)
            materialBuilder.icon(resId);

        materialBuilder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (callback != null)
                    callback.onPositiveClicked(dialog);
            }
        });

        if (negativeText != null) {
            materialBuilder.negativeText(negativeText);
            materialBuilder.onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                    if (callback != null)
                        callback.onNegativeClicked(dialog);
                }
            });
        }

        materialBuilder.show();*/
    }

    public static void showUnderDevelopmentAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Alert");
        builder.setMessage("Under Development");
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    public static class DialogCallback implements Callback {
        @Override
        public void onPositiveClicked(DialogInterface dialog) {
        }

        @Override
        public void onNegativeClicked(DialogInterface dialog) {
        }
    }

    private interface Callback {
        void onPositiveClicked(DialogInterface dialog);

        void onNegativeClicked(DialogInterface dialog);
    }
}
