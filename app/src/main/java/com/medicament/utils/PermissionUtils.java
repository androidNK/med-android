package com.medicament.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;

import com.medicament.R;
import com.medicament.manager.SharedPreferenceManager;

import java.util.Map;

/**
 * Created by LeewayHertz on 8/20/17.
 * PermissionUtils class is used to check for permission and allow or deny the permission
 */

public class PermissionUtils {

    public static final int PERMISSION_ALL = 100;
    public static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 101;
    public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;
    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 103;
    public static final int PERMISSIONS_REQUEST_CALL_PHONE = 104;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionByPermissionName(final Context context, String permission, String message, boolean isCancelable, DialogUtils.DialogCallback listener) {

        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                if (SharedPreferenceManager.getInstance().getNeverAskAgainStatus(permission)) {
                    Drawable drawableResource = getDrawableIcon(context, permission);

                    if (isCancelable)
                        showPermissionAlert(context, drawableResource, message, listener);
                    else
                        showNonCancelablePermissionAlert(context, drawableResource, message, listener);

                } else {
                    switch (permission) {
                        case Manifest.permission_group.STORAGE:
                            ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                            break;
                        case Manifest.permission_group.PHONE:
                            ActivityCompat.requestPermissions((Activity) context, new String[]{permission, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
                            break;
                        case Manifest.permission.CALL_PHONE:
                            ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, PERMISSIONS_REQUEST_CALL_PHONE);
                            break;
                    }
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkAllPermission(final Context context, String[] permissions, String message, DialogUtils.DialogCallback listener) {

        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            boolean permissionStatus = false;
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    if (SharedPreferenceManager.getInstance().getNeverAskAgainStatus(permission))
                        showPermissionAlert(context, null, message, listener);
                    else
                        ActivityCompat.requestPermissions((Activity) context, permissions, PERMISSION_ALL);

                    return false;

                } else {
                    permissionStatus = true;

                }
            }
            return permissionStatus;
        } else {
            return true;
        }

    }

    @Nullable
    public static Drawable getDrawableIcon(Context context, String permission) {
        PackageManager pm = context.getPackageManager();
        PermissionInfo info = null;
        Drawable drawableResource = null;
        try {
            info = pm.getPermissionInfo(permission, 0);
            PermissionGroupInfo groupInfo = pm.getPermissionGroupInfo(info.group, 0);
            //noinspection deprecation
            drawableResource = pm.getResourcesForApplication(context.getPackageName()).getDrawable(groupInfo.icon);
            //noinspection deprecation
            int permissionIconColor = context.getResources().getColor(R.color.colorPrimary);
            drawableResource.setColorFilter(permissionIconColor, PorterDuff.Mode.SRC_ATOP); //Important for changing the color of drawable
        } catch (PackageManager.NameNotFoundException e) {
//            LHTLogger.instance().error("Kamran", "PermissionUtils", "getDrawableIcon", e.getMessage(), Arrays.toString(e.getStackTrace()), false);
        }
        return drawableResource;
    }

    public static void showPermissionAlert(Context context, Drawable resId, String message, DialogUtils.DialogCallback listener) {
        //noinspection deprecation
        DialogUtils.showPermissionDialog(context, Html.fromHtml("<font color='#FF7F27 '>Permission Necessary</font>"), message, context.getString(R.string.settings), context.getString(android.R.string.no), resId, listener);
    }

    public static void showNonCancelablePermissionAlert(Context context, Drawable resId, String message, DialogUtils.DialogCallback listener) {
        //noinspection deprecation
        DialogUtils.showPermissionDialog(context, Html.fromHtml("<font color='#FF7F27 '>Permission Necessary</font>"), message, context.getString(R.string.settings), null, resId, listener);
    }

    public static StringBuilder getQueryString(Map<String, Object> map) {
        StringBuilder url = new StringBuilder();
        int index = 0;
        if (map == null || map.entrySet().size() == 0)
            return url;
        url.append("?");
        if (map.entrySet().size() > 1)
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                url.append(entry.getKey()).append("=").append(entry.getValue());
                index++;
                if (index != map.entrySet().size() - 1)
                    url.append("&");
            }
        else
            url.append(map.entrySet().iterator().next().getKey()).append("=").append(map.entrySet().iterator().next().getValue());
        return url;
    }

/*    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int index = 0; index < permissions.length; index++) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                if (requestCode == AppConstants.PERMISSIONS_REQUEST_STORAGE)
                    startCameraActivity();
                else
                    startCropImageActivity(mCropImageUri);
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[index]);
                if (showRationale)
                    SharedPreferenceManager.getInstance().saveNeverAskAgainStatus(permissions[index], false);
                else {
                    SharedPreferenceManager.getInstance().saveNeverAskAgainStatus(permissions[index], true);
                    if (requestCode == AppConstants.PERMISSIONS_REQUEST_STORAGE)
                        AppUtility.showPermissionAlert(this, AppUtility.getDrawableIcon(this, permissions[index]), getString(R.string.camera_permission_required_text));
                }
            }
        }
    }*/

}
